<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelStarRating extends Model
{
    public $table = "number_of_stars";
    public $timestamps = false;
}
