<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Hotel;
use App\Http\Resources\Hotel as HotelResource;

class HotelController extends Controller
{
   
    public function index()
    {
        $hotels = Hotel::get();
        return HotelResource::collection($hotels);
    }

    public function show($id)
    {
        $hotel = Hotel::findOrFail($id);
        return new HotelResource($hotel);
    }
    
    public function store(Request $request)
    {
        $hotel = new Hotel;
        $hotel->name = $request->input('name');
        $hotel->stars = $request->input('stars');
        $hotel->city = $request->input('city');
        $hotel->street = $request->input('street');
        $hotel->house_address = $request->input('house_address');
        if($hotel->save()) {
            return new HotelResource($hotel);
        }
    }

    public function update(Request $request, $id)
    {
        $hotel = Hotel::findOrFail($id);
        $hotel->name = $request->input('name');
        $hotel->stars = $request->input('stars');
        $hotel->city = $request->input('city');
        $hotel->street = $request->input('street');
        $hotel->house_address = $request->input('house_address');
        if($hotel->save()) {
            return new HotelResource($hotel);
        }
    }

    public function destroy($id)
    {
        $hotel = Hotel::findOrFail($id);
        if($hotel->delete()){
            return new HotelResource($hotel);
        }
    }
}
