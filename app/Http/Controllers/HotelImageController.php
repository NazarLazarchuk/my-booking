<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\HotelImage;
use App\Http\Resources\HotelImage as HotelImageResource;

class HotelImageController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
       $this->validate($request, [

        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4096',

        ]);

        $image = $request->file('image');

        $input['imagename'] = time().rand(1, 999).'.'.$image->getClientOriginalExtension();

        $imageObject = new HotelImage;
        $imageObject->name = $input['imagename'];
        $imageObject->hotel_id = (int)($request->input('hotel_id'));
        $destinationPath = public_path('/images');

        $image->move($destinationPath, $input['imagename']);

        if($imageObject->save()){
            return new HotelImageResource($imageObject);
        }
    }

    public function show($id)
    {
        $image = HotelImage::findOrFail($id);
        $file = public_path('/images')."/".$image->name;
        $size = getimagesize($file);
        header('Content-Type: '.$size['mime']);
        header('Content-Length: ' . filesize($file));
        readfile($file);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $image = HotelImage::findOrFail($id);
        if($image->delete()){
            unlink(public_path('/images')."/".$image->name);
            return new HotelImageResource($image);
        }
    }
}
