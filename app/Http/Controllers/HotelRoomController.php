<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\HotelRoom;
use App\Http\Resources\HotelRoom as HotelRoomResource;

class HotelRoomController extends Controller
{
   
    public function index()
    {
        //
    }

   
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $room = new HotelRoom;
        $room->hotel_id = $request->input('hotel_id');
        $room->max_adult_count = $request->input('max_adult_count');
        $room->max_child_count = $request->input('max_child_count');
        $room->max_peoples_count = $request->input('max_peoples_count');
        
        if($room->save()) {
            return new HotelRoomResource($room);
        }
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
