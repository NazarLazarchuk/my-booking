<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HotelStarRating;
use App\Http\Resources\HotelStarRating as HotelStarRatingResource;

class HotelStarRatingController extends Controller
{
    public function index()
    {
        $ratings = HotelStarRating::get();
        return HotelStarRatingResource::collection($ratings);
    }
}
