<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\RoomOrder;
use App\Http\Resources\RoomOrder as RoomOrderResource;

class RoomOrderController extends Controller
{
   
    public function index()
    {
        $orders = RoomOrder::get();
        return RoomOrderResource::collection($orders);
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        $order = new RoomOrder;
        $order->customer_id = 1;
        $order->room_id = $request->input('room_id');
        $order->start_date = $request->input('start_date')." 00:00:00";
        $order->end_date = $request->input('end_date')." 00:00:00";
        
        if($order->save()) {
            return new RoomOrderResource($order);
        }
    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
