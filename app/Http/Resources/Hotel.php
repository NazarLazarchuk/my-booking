<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\HotelImage;
use App\HotelRoom;

class Hotel extends JsonResource
{
    public function toArray($request)
    {
        $images = HotelImage::where('hotel_id', $this->id)->select('id')->get();
        $rooms = HotelRoom::where('hotel_id', $this->id)->get();
        $rooms_count = count($rooms);
        
        return [
            'id' => $this->id,
            'name' => $this->name,
            'stars' => $this->stars,
            'city' => $this->city,
            'street' => $this->street,
            'house_address' => $this->house_address,
            'images_id' => $images,
            'rooms_count' => $rooms_count,
            'rooms' => $rooms
        ];
    }
}
