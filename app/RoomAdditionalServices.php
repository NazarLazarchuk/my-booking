<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomAdditionalServices extends Model
{
    public $table = "room_additional_services";
    public $timestamps = false;
}
