<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoomOrder extends Model
{
    public $table = "room_order";
}
