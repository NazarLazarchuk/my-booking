<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomAdditionalServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_additional_services', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('wifi')->default(false);
            $table->boolean('parking_lot')->default(false);
            $table->boolean('separate_bathroom')->default(false);
            $table->integer('hotel_room_id')->unsigned()->unique();
            
            $table->foreign('hotel_room_id')->references('id')->on('hotel_room')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_additional_services');
    }
}
