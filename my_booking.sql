-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Окт 30 2018 г., 18:25
-- Версия сервера: 10.1.35-MariaDB
-- Версия PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `my_booking`
--

-- --------------------------------------------------------

--
-- Структура таблицы `hotel`
--

CREATE TABLE `hotel` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `stars` int(10) UNSIGNED NOT NULL,
  `city` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `house_address` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `hotel`
--

INSERT INTO `hotel` (`id`, `name`, `stars`, `city`, `street`, `house_address`) VALUES
(1, 'Готель Україна', 1, 'Житомир', 'Київська', '3'),
(2, 'Готель Плазма', 3, 'Львів', 'Проспект Свободи', '5'),
(4, 'Готель Мир', 3, 'Київ', 'проспект Голосіївський', '70');

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_image`
--

CREATE TABLE `hotel_image` (
  `id` int(10) UNSIGNED NOT NULL,
  `hotel_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `hotel_image`
--

INSERT INTO `hotel_image` (`id`, `hotel_id`, `name`, `created_at`, `updated_at`) VALUES
(2, 1, '1540747132628.jpg', '2018-10-28 15:18:52', '2018-10-28 15:18:52'),
(3, 4, '1540754776183.jpg', '2018-10-28 17:26:16', '2018-10-28 17:26:16'),
(4, 4, '1540754777466.jpg', '2018-10-28 17:26:17', '2018-10-28 17:26:17'),
(5, 2, '1540853132449.jpg', '2018-10-29 20:45:32', '2018-10-29 20:45:32'),
(6, 2, '1540853138445.jpg', '2018-10-29 20:45:38', '2018-10-29 20:45:38'),
(7, 2, '1540853181958.jpg', '2018-10-29 20:46:21', '2018-10-29 20:46:21'),
(8, 1, '1540920237351.JPG', '2018-10-30 15:23:57', '2018-10-30 15:23:57');

-- --------------------------------------------------------

--
-- Структура таблицы `hotel_room`
--

CREATE TABLE `hotel_room` (
  `id` int(10) UNSIGNED NOT NULL,
  `hotel_id` int(10) UNSIGNED NOT NULL,
  `max_adult_count` int(11) NOT NULL,
  `max_child_count` int(11) NOT NULL,
  `max_peoples_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `hotel_room`
--

INSERT INTO `hotel_room` (`id`, `hotel_id`, `max_adult_count`, `max_child_count`, `max_peoples_count`) VALUES
(1, 1, 2, 2, 4),
(2, 1, 2, 1, 2),
(3, 2, 2, 0, 2),
(4, 2, 2, 2, 3),
(5, 4, 1, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_10_28_083632_create_number_of_stars_table', 1),
(4, '2018_10_28_083633_create_hotel_table', 1),
(5, '2018_10_28_101450_create_hotel_room_table', 1),
(6, '2018_10_28_101738_create_room_additional_services_table', 1),
(7, '2018_10_28_101925_create_room_order_table', 1),
(8, '2018_10_28_165325_create_hotel_image_table', 2),
(9, '2018_10_28_165520_create_hotel_image_table', 3),
(10, '2018_10_28_165805_create_hotel_image_table', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `number_of_stars`
--

CREATE TABLE `number_of_stars` (
  `id` int(10) UNSIGNED NOT NULL,
  `stars_count` int(10) UNSIGNED NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `EUR_value` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `number_of_stars`
--

INSERT INTO `number_of_stars` (`id`, `stars_count`, `value`, `EUR_value`) VALUES
(1, 1, '1 зірка', 'D'),
(2, 2, '2 зірки', 'C'),
(3, 3, '3 зірки', 'B'),
(4, 4, '4 зірки', 'A'),
(5, 5, '5 зірок', 'De luxe'),
(6, 0, 'Не оцінено', 'Undefined');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `room_additional_services`
--

CREATE TABLE `room_additional_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `wifi` tinyint(1) NOT NULL DEFAULT '0',
  `parking_lot` tinyint(1) NOT NULL DEFAULT '0',
  `separate_bathroom` tinyint(1) NOT NULL DEFAULT '0',
  `hotel_room_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `room_additional_services`
--

INSERT INTO `room_additional_services` (`id`, `wifi`, `parking_lot`, `separate_bathroom`, `hotel_room_id`) VALUES
(1, 0, 1, 0, 1),
(2, 1, 1, 0, 2),
(3, 1, 1, 1, 3),
(4, 1, 1, 1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `room_order`
--

CREATE TABLE `room_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `room_id` int(10) UNSIGNED NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `room_order`
--

INSERT INTO `room_order` (`id`, `customer_id`, `room_id`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-10-29 00:00:00', '2018-11-22 00:00:00', NULL, NULL),
(2, 1, 3, '2018-10-29 00:00:00', '2018-11-20 00:00:00', NULL, NULL),
(3, 1, 2, '2018-10-31 00:00:00', '2018-11-01 00:00:00', NULL, NULL),
(4, 1, 5, '2018-10-30 00:00:00', '2018-10-31 00:00:00', '2018-10-30 15:22:02', '2018-10-30 15:22:02'),
(5, 1, 4, '2018-10-30 00:00:00', '2018-10-31 00:00:00', '2018-10-30 15:22:23', '2018-10-30 15:22:23');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_stars_foreign` (`stars`);

--
-- Индексы таблицы `hotel_image`
--
ALTER TABLE `hotel_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_image_hotel_id_foreign` (`hotel_id`);

--
-- Индексы таблицы `hotel_room`
--
ALTER TABLE `hotel_room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_room_hotel_id_foreign` (`hotel_id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `number_of_stars`
--
ALTER TABLE `number_of_stars`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `number_of_stars_stars_count_unique` (`stars_count`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `room_additional_services`
--
ALTER TABLE `room_additional_services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `room_additional_services_hotel_room_id_unique` (`hotel_room_id`);

--
-- Индексы таблицы `room_order`
--
ALTER TABLE `room_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `room_order_room_id_foreign` (`room_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `hotel_image`
--
ALTER TABLE `hotel_image`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `hotel_room`
--
ALTER TABLE `hotel_room`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `number_of_stars`
--
ALTER TABLE `number_of_stars`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `room_additional_services`
--
ALTER TABLE `room_additional_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `room_order`
--
ALTER TABLE `room_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `hotel`
--
ALTER TABLE `hotel`
  ADD CONSTRAINT `hotel_stars_foreign` FOREIGN KEY (`stars`) REFERENCES `number_of_stars` (`id`);

--
-- Ограничения внешнего ключа таблицы `hotel_image`
--
ALTER TABLE `hotel_image`
  ADD CONSTRAINT `hotel_image_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `hotel_room`
--
ALTER TABLE `hotel_room`
  ADD CONSTRAINT `hotel_room_hotel_id_foreign` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `room_additional_services`
--
ALTER TABLE `room_additional_services`
  ADD CONSTRAINT `room_additional_services_hotel_room_id_foreign` FOREIGN KEY (`hotel_room_id`) REFERENCES `hotel_room` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `room_order`
--
ALTER TABLE `room_order`
  ADD CONSTRAINT `room_order_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `hotel_room` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
