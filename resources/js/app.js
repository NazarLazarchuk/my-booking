require('./bootstrap');
window.Vue = require('vue');


const Navbar = require('./components/Navbar.vue');
Vue.component('custom-navbar', Navbar);

const Footer = require('./components/Footer.vue');
Vue.component('custom-footer', Footer);

const HomePage = require('./components/HomePage.vue');
Vue.component('homepage', HomePage);

const HotelPage = require('./components/HotelPage.vue');
Vue.component('hotel-page', HotelPage);

const Carousel = require('./components/Carousel.vue');
Vue.component('carousel', Carousel);

const CreateHotel = require('./components/CreateHotel.vue');
Vue.component('create-hotel', CreateHotel);

const CreateRoom = require('./components/CreateRoom.vue');
Vue.component('create-room', CreateRoom);

const VueRouter = require('vue-router').default;
Vue.use(VueRouter);

const routes = [
    { path: '/', redirect: '/home' },
    { path: '/home', component: HomePage },
    { path: '/create-hotel', component: CreateHotel },
    { path: '/create-room', component: CreateRoom },
    { path: '/hotel/:id/:start_date/:end_date', component: HotelPage, props: true },
    { path: '/hotel/:id', component: HotelPage, props: true }
];

const router = new VueRouter({
    mode: 'history',
    routes
});

const app = new Vue({
    el: '#app',
    router
});
