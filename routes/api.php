<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/hotels', 'HotelController@index');
Route::get('/hotel/{id}', 'HotelController@show');
Route::post('/hotel', 'HotelController@store');
Route::put('/hotel/{id}', 'HotelController@update');
Route::delete('/hotel/{id}', 'HotelController@destroy');

Route::post('image', 'HotelImageController@store');
Route::get('image/{id}', 'HotelImageController@show');
Route::delete('image/{id}', 'HotelImageController@destroy');

Route::get('ratings', 'HotelStarRatingController@index');

Route::get('orders', 'RoomOrderController@index');
Route::post('orders', 'RoomOrderController@store');

Route::post('room', 'HotelRoomController@store');

